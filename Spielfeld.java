import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

class Spielfeld extends Canvas implements MouseListener {
    //Anfang Attribute
    int[][] spielstand = new int[3][3];
    int zugleft = 9;
    boolean pcErsterZug = false;
    TicTacToe masterframe = null;
    int pgSize = 0;
    int itemSize = (int)(pgSize / 7.5);
    //Ende Atribute

    //Anfang Methoden
    public Spielfeld(TicTacToe masterframe) {
        super();
        this.masterframe = masterframe;
        addMouseListener(this);
        this.pgSize = pgSize;
    }
    //MouseListener Methoden
    public void mouseExited(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    public void mousePressed(MouseEvent e) {}
    public void mouseClicked(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (x > j * pgSize / 3 && x < (j + 1) * pgSize / 3 && y > i * pgSize / 3 && y < (i + 1) * pgSize / 3 && zugleft != 0 && spielstand[j][i] == 0) {
                    if (pcErsterZug) {
                        spielstand[j][i] = -1;
                    } else {
                        spielstand[j][i] = 1;
                    }
                    zugleft--;
                    //          System.out.println(zugPosibilities(geklickt, zugleft));
                    gewinnerAusgabe();
                    if (zugleft != 0) {
                        computer(zugleft);
                        zugleft--;
                        //          System.out.println(zugPosibilities(geklickt, zugleft));
                        gewinnerAusgabe();
                    }
                    repaint();
                }
            }
        }
    }
    //Paint Methoden
    public void paint(Graphics g) {
        g.drawLine(pgSize / 3, 0, pgSize / 3, pgSize);
        g.drawLine(2 * pgSize / 3, 0, 2 * pgSize / 3, pgSize);
        g.drawLine(0, pgSize / 3, pgSize, pgSize / 3);
        g.drawLine(0, 2 * pgSize / 3, pgSize, 2 * pgSize / 3);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (spielstand[j][i] == 1) {
                    drawX(g, j * pgSize / 3 + pgSize / 6, i * pgSize / 3 + pgSize / 6);
                }
                if (spielstand[j][i] == -1) {
                    drawCircle(g, j * pgSize / 3 + pgSize / 6, i * pgSize / 3 + pgSize / 6);
                }
            }
        }
    }
    private void drawX(Graphics g, int x, int y) {
        g.drawLine(x - itemSize, y - itemSize, x + itemSize, y + itemSize);
        g.drawLine(x - itemSize, y + itemSize, x + itemSize, y - itemSize);
    }
    private void drawCircle(Graphics g, int x, int y) {
        g.drawOval(x - itemSize, y - itemSize, itemSize * 2, itemSize * 2);
    }
    //Game methoden
    private void computer(int zugleft) {
        int computerXoderO = -1;
        if (pcErsterZug) {
            computerXoderO = 1;
        }
        int spielstandTemp[][] = new int[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                spielstandTemp[i][j] = spielstand[i][j];
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (spielstand[i][j] == 0) {
                    spielstandTemp[i][j] = computerXoderO;
                    if (zugPosibilities(spielstandTemp, zugleft - 1) == computerXoderO) {
                        spielstand[i][j] = computerXoderO;
                        repaint();
                        return;
                    }
                    spielstandTemp[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (spielstand[i][j] == 0) {
                    spielstandTemp[i][j] = computerXoderO;
                    if (zugPosibilities(spielstandTemp, zugleft - 1) == 0) {
                        spielstand[i][j] = computerXoderO;
                        repaint();
                        return;
                    }
                    spielstandTemp[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (spielstand[i][j] == 0) {
                    spielstandTemp[i][j] = computerXoderO;
                    if (zugPosibilities(spielstandTemp, zugleft - 1) == -computerXoderO) {
                        spielstand[i][j] = computerXoderO;
                        repaint();
                        return;
                    }
                    spielstandTemp[i][j] = 0;
                }
            }
        }
    }
    private int zugPosibilities(int[][] spielstandUebergeben, int zugleft) {
        //arrt=new int[3][3];
        int computerXoderO = -1;
        if (pcErsterZug) {
            computerXoderO = 1;
        }
        boolean SpielerAmZug;
        int spielerstandUebergebenTemp[][] = new int[3][3];
        int moeglicheZugPossibilites[] = new int[zugleft];
        SpielerAmZug = zugleft % 2 == 1;
        if (pcErsterZug) {
            SpielerAmZug = !SpielerAmZug;
        }
        int checkwintemp = checkwin(spielstandUebergeben, !SpielerAmZug);
        if (checkwintemp != 0) {
            return checkwintemp;
        }
        int temp = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                spielerstandUebergebenTemp[i][j] = spielstandUebergeben[i][j];
            }
        }
        if (zugleft == 0) {
            return checkwintemp;
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (spielstandUebergeben[i][j] == 0) {
                    if (SpielerAmZug) {
                        spielerstandUebergebenTemp[i][j] = -computerXoderO;
                    } else {
                        spielerstandUebergebenTemp[i][j] = computerXoderO;
                    }
                    moeglicheZugPossibilites[temp] = zugPosibilities(spielerstandUebergebenTemp, zugleft - 1);
                    spielerstandUebergebenTemp[i][j] = 0;
                    temp++;
                }


            }
        }
        if (zugleft % 2 == 1) {
            temp = -1;
            for (int i = 0; i < zugleft; i++) {
                if (moeglicheZugPossibilites[i] > temp) {
                    temp = moeglicheZugPossibilites[i];
                }
            }

        } else {
            temp = 1;
            for (int i = 0; i < zugleft; i++) {
                if (moeglicheZugPossibilites[i] < temp) {
                    temp = moeglicheZugPossibilites[i];
                }
            }

        }
        return temp;

    }
    private int checkwin(int[][] temparr, boolean zugt) {
        if (pcErsterZug) {
            zugt = !zugt;
        }
        for (int i = 0; i < 3; i++) {
            if ((temparr[i][0] == 1 && temparr[i][1] == 1 && temparr[i][2] == 1) || (temparr[i][0] == -1 && temparr[i][1] == -1 && temparr[i][2] == -1)) {
                if (zugt) {
                    return 1;
                } else {
                    return -1;
                }
            }
            if ((temparr[0][i] == 1 && temparr[1][i] == 1 && temparr[2][i] == 1) || (temparr[0][i] == -1 && temparr[1][i] == -1 && temparr[2][i] == -1)) {
                if (zugt) {
                    return 1;
                } else {
                    return -1;
                }
            }

        }
        if ((temparr[0][0] == 1 && temparr[1][1] == 1 && temparr[2][2] == 1) ||
            (temparr[0][2] == -1 && temparr[1][1] == -1 && temparr[2][0] == -1) ||
            (temparr[0][0] == -1 && temparr[1][1] == -1 && temparr[2][2] == -1) ||
            (temparr[0][2] == 1 && temparr[1][1] == 1 && temparr[2][0] == 1)) {
            if (zugt) {
                return 1;
            } else {
                return -1;
            }
        }
        return 0;
    }
    private void gewinnerAusgabe() {
        if (checkwin(spielstand, zugleft % 2 == 0) == 1) {
            masterframe.setTextField1("Du hat gewonnen");
            zugleft = 0;
        } else if (checkwin(spielstand, zugleft % 2 == 0) == -1) {
            masterframe.setTextField1("Computer hat gewonnen");
            zugleft = 0;
        } else if (zugleft == 0) {
            masterframe.setTextField1("Unentschieden");
        }
    }
    //Public Methoden
    public void resetSpielfeld() {
        spielstand = new int[3][3];
        zugleft = 9;
        repaint();
        if (pcErsterZug) {
            computer(zugleft);
            zugleft--;
        }
    }
    public void anfangWechsel() {
        pcErsterZug = !pcErsterZug;
        resetSpielfeld();
    }
    public void aendereGroesse(int pgSize) {
        this.pgSize = pgSize;
        itemSize = (int)(this.pgSize / 7.5);
    }
    //Ende Methoden
}