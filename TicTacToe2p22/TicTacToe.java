﻿import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import static java.lang.Math.min;


public class TicTacToe extends JFrame {
    // Anfang Attribute
    private Spielfeld canvas1 = new Spielfeld(this);
    private JButton jButton1 = new JButton();
    private JButton jButton2 = new JButton();
    private JTextField jTextField1 = new JTextField();
    private int groesseFensterX = 340;
    private int groesseFensterY = 410;
    // Ende Attribute

    public TicTacToe() {
        // Frame-Initialisierung
        super();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        int frameWidth = groesseFensterX;
        int frameHeight = groesseFensterY;
        setSize(frameWidth, frameHeight);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (d.width - getSize().width) / 2;
        int y = (d.height - getSize().height) / 2;
        setLocation(x, y);
        setTitle("TicTacToe");
        setResizable(false);
        Container cp = getContentPane();
        cp.setLayout(null);
        // Anfang Komponenten

        canvas1.setBounds(0, 0, this.groesseFensterX, this.groesseFensterY - 110);
        cp.add(canvas1);
        jButton1.setBounds(15, 315, 99, 25);
        jButton1.setText("Neue Runde");
        jButton1.setMargin(new Insets(2, 2, 2, 2));
        jButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jButton1_ActionPerformed(evt);
            }
        });
        cp.add(jButton1);
        jTextField1.setBounds(125, 326, 150, 20);
        cp.add(jTextField1);

        jButton2.setBounds(15, 340, 99, 25);
        jButton2.setText("Spielerwechsel");
        jButton2.setMargin(new Insets(2, 2, 2, 2));
        jButton2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jButton2_ActionPerformed(evt);
            }
        });
        cp.add(jButton2);
        // Ende Komponenten
        canvas1.aendereGroesse(min(groesseFensterX, groesseFensterY - 110));
        setVisible(true);
    }

    // Anfang Methoden

    public static void main(String[] args) {
        new TicTacToe();
    }
    public void jButton1_ActionPerformed(ActionEvent evt) {
        // TODO hier Quelltext einfuegen
        canvas1.resetSpielfeld();
        jTextField1.setText(null);
    }

    public void jButton2_ActionPerformed(ActionEvent evt) {
        // TODO hier Quelltext einfuegen
        canvas1.anfangWechsel();
    }

    public void setTextField1(String text) {
        jTextField1.setText(text);
    }

    // Ende Methoden
}